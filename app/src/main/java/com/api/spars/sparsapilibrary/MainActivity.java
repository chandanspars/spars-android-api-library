package com.api.spars.sparsapilibrary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.api.spars.sparsapilibrary.networking.ApiClients;

import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Retrofit apiClients= ApiClients.getApiClient();
    }
}
