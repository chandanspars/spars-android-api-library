package com.api.spars.sparsapilibrary.networking;

import com.squareup.moshi.Moshi;
import com.squareup.moshi.Rfc3339DateJsonAdapter;

import java.io.IOException;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by chandan on 1/2/18.
 */

public class ApiClients {

    private static Retrofit apiClient;

    private static Boolean shouldUpdateHeader = false;

    private static String accessToken;
    private static String baseUrl;

    public static synchronized Retrofit getApiClient() {
        if (apiClient == null || shouldUpdateHeader) {
            shouldUpdateHeader = false;
            OkHttpClient client = getOKHttpBuilder().build();
            Moshi.Builder builder = new Moshi.Builder();
            registerMoshiAdapters(builder);
            Moshi moshi = builder.build();
            apiClient = new Retrofit
                    .Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                    .client(client)
                    .build();
        }
        return apiClient;
    }

    private static OkHttpClient.Builder getOKHttpBuilder(){
        OkHttpClient.Builder builder;
        builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(new Interceptor() {
            @Override public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .addHeader("access_token", accessToken)
                        .build();
                return chain.proceed(request);
            }
        });
        return builder;
    }

    //  register all json adapters here
    private static void registerMoshiAdapters(Moshi.Builder builder){
        builder.add(Date.class, new Rfc3339DateJsonAdapter().nullSafe());
    }

    public static void updateHeaders(){
        shouldUpdateHeader = true;
    }

    public static void setAccessToken (String token) {
        accessToken = token;
    }

    public static void setBaseUrl (String url) {
        baseUrl = url;
    }

}
